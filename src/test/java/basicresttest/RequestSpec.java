package basicresttest;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class RequestSpec {

    RequestSpecification requestSpecification;

    @BeforeTest
    public void setRequestParams(){

        requestSpecification = RestAssured.given().baseUri("https://reqres.in/").basePath("api/");
    }

    @Test
    public void getListUser(){

      Response UserListResponse =  RestAssured.given().spec(requestSpecification).queryParam("page", 2).when().get("users");
      UserListResponse.prettyPrint();

    }

    @Test
    public void getSingleUserAPI(){

       Response UserResponse =  RestAssured.given(requestSpecification).when().get("users/2");
       UserResponse.prettyPrint();
    }

    @Test
    public void getSingleResource(){

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        requestSpecBuilder.setBaseUri("https://reqres.in/api/unknown/2");
        RequestSpecification requestSpecification =requestSpecBuilder.build();

      Response response = RestAssured.given().spec(requestSpecification).get();
        System.out.println(response.asString());
        System.out.println(response.getHeader("age"));
        Headers headers = response.headers();
        System.out.println(headers);
//        requestSpecification.log().all().get();
       // response.prettyPrint();
    }


}
