package basicresttest.newreq;

public class Payload {

    String one;
    String two;
    Yes yes;

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public Yes getYes() {
        return yes;
    }

    public void setYes(Yes yes) {
        this.yes = yes;
    }
}
