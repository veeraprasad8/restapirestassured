package basicresttest.newreq;

public class Yes {

    String yek;
    Integer arr[];
    Student student[];

    public String getYek() {
        return yek;
    }

    public void setYek(String yek) {
        this.yek = yek;
    }

    public Integer[] getArr() {
        return arr;
    }

    public void setArr(Integer[] arr) {
        this.arr = arr;
    }

    public Student[] getStudent() {
        return student;
    }

    public void setStudent(Student[] student) {
        this.student = student;
    }
}
