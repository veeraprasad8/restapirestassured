package basicresttest;

import apibase.ApiCommon;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.RestAssured.*;

public class ApiTest extends ApiCommon {

    RequestSpecification requestSpecification;

//    @BeforeTest
//    public void settingApiParams(){
//        baseURI = "https://reqres.in/";
//        basePath = "api/";
//
//         requestSpecification = RestAssured.given().headers("Content-Type", "application/json");
//
//
//
//
//
//    }


    @Test
    public void createUserPostAPI() throws Exception {


        HashMap<String, Object> requestBody = new HashMap<String, Object>();
        requestBody.put("name", "test");
        requestBody.put("job", "testing");

        HashMap<String, Object> requestBody2 = new HashMap<String, Object>();
        requestBody2.put("name", "test1");
        requestBody2.put("job", "testing1");
        requestSpecification.log().all().body(requestBody).when().post("users").then().log().all().assertThat()
                .body(matchesJsonSchema(new FileReader(new File("C:\\Users\\Sai Ram\\workspace\\apidemo\\RestAssuredTest\\schema1.json"))));


//        JsonPath jsonPath = new JsonPath(myJson.asString());
//
//        String actId = jsonPath.get("name");
//        String expID = "nv";
//        Assert.assertEquals(actId, expID, "failing due to name key doesnot math");


    }

    @Test
    public void getSingleUserAPI() throws Exception{


        Response json = RestAssured.given().spec(setParams()).get();
        json.prettyPeek();



    }


    @Test
    public void createEmp(){

        Employee employee = new Employee("veeratest", "apitester");
//        employee.setName("veera");
//        employee.setJob("tester");
       Response response =  requestSpecification.body(employee).contentType("application/json").log().all().post("users");


       response.prettyPeek();
       EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);

        System.out.println(employeeResponse.getCreatedAt());
        System.out.println(employeeResponse.getId());


    }


    @Test
    public void createEmp1() throws Exception{

        Repository repository = new Repository();
        repository.setName("tetrepo");
        repository.setDescription("this is test repo");
        repository.setHomepage("https://github.com");
        Repo repo = new Repo();
        repo.setReponame("repotet");
        repo.setRepoId("repo123");
        repository.setRepo(repo);



        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(objectMapper.writeValueAsString(repository));



//
//        Response response =  requestSpecification.body(repository).contentType("application/json").log().all().post("users");
//
//
//        response.prettyPeek();
//        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);
//
//        System.out.println(employeeResponse.getCreatedAt());
//        System.out.println(employeeResponse.getId());


    }




}
