package githubtestapis;

import apibase.ApiCommon;
import apibase.ApiEndPoints;
import apibase.reponsepojo.CreateRepoOwnerResponse;
import apibase.reponsepojo.CreateRepoResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import repositorypojorequest.CreateRepo;

import java.util.Random;


public class GithubGetApisTest extends ApiCommon {

    public static Response repoResponse;

    public CreateRepo createRepo = null;


    @Test(description = "getting all the repos for authorized user")
    public void getAllReposTest() throws Exception{


       repoResponse =  RestAssured.given().spec(setParams()).get(ApiEndPoints.REPO);

        repoResponse.then().statusCode(200);

    }

    @Test(description = "creating repo for authorized user")
    public void createNewReposTest() throws Exception{


        createRepo = new CreateRepo();
        Random random = new Random();
        int upperBound = 100;
        int random_Number = random.nextInt(upperBound);
        String repoName = "testRepo"+random_Number;
        createRepo.setName(repoName);
        createRepo.setDescription("creating test repository for testing");
        createRepo.setHomepage("https://github.com");

        repoResponse =  RestAssured.given().spec(setParams()).body(createRepo).post(ApiEndPoints.REPO);

        repoResponse.then().statusCode(201);

        System.out.println(repoResponse.asString());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        CreateRepoResponse createRepoResponse = repoResponse.as(CreateRepoResponse.class);
//       //CreateRepoOwnerResponse createRepoOwnerResponse =  createRepoResponse.getOwner();
//        System.out.println(createRepoResponse.getName());

        CreateRepoResponse createRepoResponse = objectMapper.readValue(repoResponse.asString(), CreateRepoResponse.class);
        System.out.println(createRepoResponse.getName());
        System.out.println(createRepoResponse.getNode_id());
        System.out.println(createRepoResponse.isPrivateField());















    }

}
