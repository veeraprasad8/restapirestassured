package repositorypojorequest;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CreateRepo {

    private String name;
    private String description;
    @JsonIgnore
    private String homepage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
}
