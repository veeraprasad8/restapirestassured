package apibase;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApiCommon {
    public Properties properties;

    public String getProperties(String keyName) throws IOException {

        try {

            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Sai Ram\\workspace\\apidemo\\RestAssuredTest\\src\\main\\config\\application-config.properties");
            properties = new Properties();
            properties.load(fileInputStream);
        } catch (FileNotFoundException f) {

            f.printStackTrace();
        }


        return properties.get(keyName).toString();

    }


    public RequestSpecification setParams() throws Exception {

     return  RestAssured.given().log().all().baseUri(getProperties("hostname"))
                .headers(setHeaders());
    }



    public Map<String, String> setHeaders()
    {

        Map<String , String> appHeaders = new HashMap<String, String>();
         appHeaders.put("Authorization", "Bearer 8380518e5e0c287c0c4b665f316074fee549151b");
         appHeaders.put("Content-Type","application/json");
         return appHeaders;

    }


}
