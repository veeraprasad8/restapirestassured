package apibase.reponsepojo;

public class CreateRepoOwnerResponse {




    private String login;

    private int id;

    private String node_id;

    private String avatar_url;

    private String gravatar_id;

    public void setLogin(String login){
        this.login = login;
    }
    public String getLogin(){
        return this.login;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setNode_id(String node_id){
        this.node_id = node_id;
    }
    public String getNode_id(){
        return this.node_id;
    }
    public void setAvatar_url(String avatar_url){
        this.avatar_url = avatar_url;
    }
    public String getAvatar_url(){
        return this.avatar_url;
    }
    public void setGravatar_id(String gravatar_id){
        this.gravatar_id = gravatar_id;
    }
    public String getGravatar_id(){
        return this.gravatar_id;
    }

}
