package apibase.reponsepojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CreateRepoResponse {


    private int id;

    private String node_id;

    private String name;

    private String full_name;

    @JsonProperty("private")
    private boolean privateField;
//
//
//
//@JsonIgnore
//    private CreateRepoOwnerResponse owner;
//@JsonIgnore
//private Html_Url html_url;
//@JsonIgnore
//private Permissions permissions;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setNode_id(String node_id){
        this.node_id = node_id;
    }
    public String getNode_id(){
        return this.node_id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setFull_name(String full_name){
        this.full_name = full_name;
    }
    public String getFull_name(){
        return this.full_name;
    }


//    public void setOwner(CreateRepoOwnerResponse owner){
//        this.owner = owner;
//    }
//    public CreateRepoOwnerResponse getOwner(){
//        return this.owner;
//    }

    public boolean isPrivateField() {
        return privateField;
    }

    public void setPrivateField(boolean privateField) {
        this.privateField = privateField;
    }

//    public Html_Url getHtml_url() {
//        return html_url;
//    }
//
//    public void setHtml_url(Html_Url html_url) {
//        this.html_url = html_url;
//    }
//
//    public Permissions getPermissions() {
//        return permissions;
//    }
//
//    public void setPermissions(Permissions permissions) {
//        this.permissions = permissions;
//    }
}
